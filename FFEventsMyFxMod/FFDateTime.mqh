//+------------------------------------------------------------------+
//|                                                   MyDateTime.mqh |
//|                                                      nicholishen |
//|                                   www.reddit.com/u/nicholishenFX |
//+------------------------------------------------------------------+
#property copyright "nicholishen"
#property link      "www.reddit.com/u/nicholishenFX"
#property version   "1.00"
#property strict
#include <Tools\DateTime.mqh>
#include "StringEnhanced.mqh"
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
struct CFFDatetime : public CDateTime
{
   int last_ajd;
   CFFDatetime():last_ajd(0){}
   CFFDatetime(datetime time):last_ajd(0){ DateTime(time);}
   
   datetime DateTime(void) const              
   { 
      string time = string(year)+"."+string(mon)+"."+string(day)+
                 " "+string(hour)+":"+string(min)+":"+string(sec);
      return StringToTime(time);                                     
   }
   void DateTimeAdj(const string date)
   {
      // convert xml time strings to mql datetime
      MqlDateTime t;
      String d = date;
      t.year= int(d.Substr(0,4));
      t.mon = MonthNum(d.Substr(6,d.Find(6,",")-6-3));
      t.day = int(d.Substr(d.Find(6,",")-2,2));
      
      //d=time;
      int pos = d.Find(0,":");
      t.hour= int(d.Substr(pos-2,2));
      t.min = int(d.Substr(pos+1,2));
      t.sec = 0;
      //string AP = d.Substr(d.Len()-2);
      //StringToUpper(AP);
      //t.hour = AP == "PM" && t.hour != 12 ? t.hour + 12 : t.hour;
      datetime dtime = StructToTime(t); // event time in mql time
      // adjust xml (GMT) time to broker/terminal time 
      
      datetime gt = TimeGMT();
      datetime st = TimeCurrent();
      int      delta    = int((st-gt)%3600);
      st       = delta >= 1800 ? st+(3600-delta): delta<-1800 ? st+(-3600-delta) : st-delta;
      delta     = int(st - gt);
      
      dtime+=delta;
      TimeToStruct(dtime,this); 
      return;
   
      
//      
//      MqlDateTime gmtStruct,nowStruct;
//      datetime GMT = TimeGMT(gmtStruct);
//      datetime NOW = TimeCurrent(nowStruct);
//      gmtStruct.sec = 0;
//      nowStruct.sec = 0;
//      GMT = StructToTime(gmtStruct);
//      NOW = StructToTime(nowStruct);
//      
//      int gmt = (int)GMT;//;-(60*60); // debug purposes   
//      if(gmt == (int)NOW)
//      {
//         TimeToStruct(dtime,this); 
//         return;
//      }
//      if(last_ajd != 0)
//      {
//         dtime += last_ajd;
//         TimeToStruct(dtime,this); 
//         return;
//      }
//      int timeadj = (int)NOW-gmt;
//      if(MathAbs(timeadj) > 12*60*60)
//      {
//         //Print(__FUNCTION__+" Time ajd hrs = "+string(MathAbs(timeadj/(60*60))));
//         TimeToStruct(dtime,this); 
//         return;
//      }
//      dtime+=timeadj;
//      TimeToStruct(dtime,this);
//      last_ajd = timeadj;
   }
   int MonthNum(string month)
   {
      if(month == "January")return 1;
      if(month == "February")return 2;
      if(month == "March")return 3;
      if(month == "April")return 4;
      if(month == "May")return 5;
      if(month == "June")return 6;
      if(month == "July")return 7;
      if(month == "August")return 8;
      if(month == "September")return 9;
      if(month == "October")return 10;
      if(month == "November")return 11;
      if(month == "December")return 12;
      return 0;
   }   
};
