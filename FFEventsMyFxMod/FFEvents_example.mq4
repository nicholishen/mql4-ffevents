//+------------------------------------------------------------------+
//|                                                FFCal_example.mq4 |
//|                                                      nicholishen |
//|                                   www.reddit.com/u/nicholishenFX |
//+------------------------------------------------------------------+
#property copyright "nicholishen"
#property link      "www.reddit.com/u/nicholishenFX"
#property version   "1.00"
#property strict
#property script_show_inputs
#include "FFEvents.mqh"

input string f1 = "USDJPY";      //Filter 1
input string f2 = "EUR";         //Filter 2
input string f3 = "High";        //Filter 3
input string f4 = "Impact_Low";  //Filter 4
input string f5 = "";            //Filter 5
input string f6 = "";            //Filter 6
input string f7 = "";            //Filter 7
input string f8 = "";            //Filter 8

enum FLAG_TYPE{IMPACT,CURRENCY};
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
{
//---
   String str;
   
   //--- declare CFFEvents objects. w&w/o param constructor. 
   CFFEvents   e1("This is just a note to add to object that outputs by calling Name()"), 
               e2;
  
   ///initialize, download, and parse calendar data
   if(!e1.OnInit())
      return;
   /*
   Note: OnInit only need to be called once! The first object clones itself along with all 
   unfiltered events into a static private object, m_events. If any action is called upon 
   an object of CFFEvents (after the first class instance initialization) before it has been explicitly assigned events by OnInit, CopyEvents,
   AddEvents, or the overloaded operators then the object will clone itself from the master list. 
   In order words, it's pretty hard to mess up.
   */
   /// example:
   str="e2 hasn't been explicitly assigned events so e2 has cloned {} events from the original.";
   str.Format(e2.Total()).MessageBox();
   e2.Sort();
   PrintEvents(e2);
   e2.Clear();
   /// e2 now is completely reset and has 0 events. This is not useful for anything other than demo/debug
   
   /// e2 will once again inherit its events from the master static object.
   str="e2 inherited {} and filtered them down to {} events";
   str.Format(e2.Total());
   str.Format(e2.FilterByImpactFlags(IMPACT_HIGH|IMPACT_MEDIUM).Total());
   str.MessageBox();
   
   /// assign all events from one object to another
   e1=e2;
   
   /// reset event filters defaults to all events
   e2.FilterReset();
   
   /// filter events by time remaining until event (in minutes)
   str = "There are {} events in the next 12 hrs";
   str.Format(e2.FilterByMinUntil(12*60).Total());
   str.MessageBox();
   
   /// accessing the events via pointers. e2[index] returns the point to the event object at index
   for(int i=0;i<e2.Total();i++)
      e2[i].ToStringEnhanced().Print();
   (str="Events were printed to the log.").MessageBox();
   
   //--- method chaining filter/sort methods using ENUMS as multiple flags
   e1.Name("Filtered by multiple flags");
   e1.FilterByCurrencyFlags(USD|CAD|JPY|MXN)
     .FilterByImpactFlags(IMPACT_HIGH|IMPACT_MEDIUM)
     .Sort(SORT_TIME);
   PrintEvents(e1);    
   
   //--- adding missing events to an object using the += operator
   e2.Name("Adding \"m/m\" keyword events to e1");
   e2.FilterByKeyword("m/m");
   e2 += e1;
   PrintEvents(e2);

   //--- reset filters to default to all events and filter by a list of keywords in the title of event
   e1.FilterReset();
   string filter_words[]={"Unemployment","m/m","y/y"};
   e1.FilterByKeyword(filter_words).Name("Filtered by list of keywords");
   PrintEvents(e1);

   //--- using user inputs to filter events
   e1.Name("Filtered by input list");
   e1.FilterReset();
   e1.FilterByCurrencyFlags(GetFlags(CURRENCY)).FilterByImpactFlags(GetFlags(IMPACT));
   PrintEvents(e1);
   
   //--- working with event data --- creating custom filter --- example: events with higher forecast than prev
   e1.FilterReset();
   e1.Name("Filtered by events with higher forecast than previous");
   for(int i=e1.Total()-1;i>=0;i--) // iterate backwards over events
   {
      if(e1[i].Forecast() == 0 || e1[i].Previous() == 0)
         e1.Delete(i); 
      else 
      if(e1[i].Forecast() <= e1[i].Previous())
         e1.Delete(i);
   }
   PrintEvents(e1);
   
   //--- working with individual events
   e1.FilterReset();
   e1.FilterByPair("USDJPY").FilterByImpactFlags(IMPACT_HIGH|IMPACT_MEDIUM);
   e1.Sort(SORT_TIME);
   Event *next_event = NULL;
   if(e1.Total() > 0)
      next_event = e1[0];
   if(next_event != NULL)
   {
      (str="This is a single event example").Print();
      str="The next event is the {} announcent, {} minutes from now.";
      str.Format(next_event.Title(),next_event.MinUntil()).Print();
      next_event.ToStringEnhanced().Print();
   }
}  
//+------------------------------------------------------------------+
void PrintEvents(CFFEvents &events)
{
   String c,p;
   (p="==================== {} ====================").Format(events.Name()).Print();
   for(int i=0;i<events.Total();i++)
   {
      (p=events[i].ToString()).Print();
      c+=p.Str()+"\r\n";
   }
   c.Comment();
}
//+------------------------------------------------------------------+
int GetFlags(FLAG_TYPE type)
{
   string str[8];
   str[0]=f1; str[1]=f2; str[2]=f3; str[3]=f4; 
   str[4]=f5; str[5]=f6; str[6]=f7; str[7]=f8;
   int flags=0;
   for(int i=0;i<ArraySize(str);i++)
   {
      if(type==CURRENCY && StringToCurrency(str[i])>0)  
         flags|=StringToCurrency(str[i]);
      else
      if(type==IMPACT && StringToImpact(str[i])>0)
         flags|=StringToImpact(str[i]);
      else
      if(type==CURRENCY && StringLen(str[i])==6)
      {
         string bc = StringSubstr(str[i],0,3);
         string cc = StringSubstr(str[i],3,3);
         if(StringToCurrency(bc)>0)
            flags|=StringToCurrency(bc);
         if(StringToCurrency(cc)>0)
            flags|=StringToCurrency(cc);
      }
   }
   return flags;
}