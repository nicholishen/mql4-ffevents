//+------------------------------------------------------------------+
//|                                                  EventParse.mqh |
//|                                                      nicholishen |
//|                                   www.reddit.com/u/nicholishenFX |
//+------------------------------------------------------------------+
#property copyright "nicholishen"
#property link      "www.reddit.com/u/nicholishenFX"
#property version   "1.00"
#property strict
#include "StringEnhanced.mqh"
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class EventParse : public String
{
private:

public:
                     EventParse();
                    ~EventParse();
   string            ParseEvent(string);
   datetime          EventDateTime(string,string);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
EventParse::EventParse()
{
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
EventParse::~EventParse()
{
}
//+------------------------------------------------------------------+

string EventParse::ParseEvent(const string start_tag)
{
   String res = start_tag;
   res.Insert(1,"/");
   string end_tag = res.Str();
   if(Find(0,start_tag)<0)
      return "";
   int begin= Find(0,start_tag)+StringLen(start_tag);
   int end  = Find(0,end_tag);
   res = Substr(begin,end-begin);
   res-= "<![CDATA[ ";
   res-= " ]]>";
   res-= "<![CDATA[";
   res-= "]]>";
   string debug = res.Str();
   return res.Str();
}
//+------------------------------------------------------------------+
//datetime EventParse::StringsToDatetime(const string date,const string time)
//{
//   MqlDateTime t;
//   String d = date;
//   t.mon = int(d.Substr(0,2));
//   t.day = int(d.Substr(3,2));
//   t.year= int(d.Substr(6,4));
//   d=time;
//   int pos = d.Find(0,":");
//   t.hour= int(d.Substr(0,pos));
//   t.min = int(d.Substr(pos+1,2));
//   t.sec = 0;
//   string AP = d.Substr(d.Len()-2);
//   StringToUpper(AP);
//   t.hour = AP == "PM" && t.hour != 12 ? t.hour + 12 : t.hour;
//   return StructToTime(t);
//}