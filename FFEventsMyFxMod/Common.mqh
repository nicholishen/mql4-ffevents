//+------------------------------------------------------------------+
//|                                                      Defines.mqh |
//|                                                      nicholishen |
//|                                   www.reddit.com/u/nicholishenFX |
//+------------------------------------------------------------------+
#property copyright "nicholishen"
#property link      "www.reddit.com/u/nicholishenFX"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
#define BIT(x) (1<<x)
//+------------------------------------------------------------------+
#ifndef __EVENTSORT__
#define __EVENTSORT__
#define SORT_TIME    0
#define SORT_IMPACT  1
#define SORT_CURRENCY 2
#endif 
//+------------------------------------------------------------------+
#ifndef __IMPACT__
#define __IMPACT__
enum ENUM_IMPACT
{
   IMPACT_HIGH    = 1,
   IMPACT_MEDIUM  = 2,
   IMPACT_LOW     = 4,
   IMPACT_HOLIDAY = 8,
   IMPACT_NULL    = -1
};
#endif 
//+------------------------------------------------------------------+
#ifndef __CURRENCY__
#define __CURRENCY__
enum ENUM_CURRENCY
{
   USD = 1,
   EUR = 2,
   CHF = 4,
   JPY = 8,
   GBP = 16,
   AUD = 32,
   NZD = 64,
   CAD = 128,
   ZAR = 256,
   SEK = 512,
   HUF = 1024,
   CNY = 2048,
   MXN = 4096,
   TRY = 8192,
   SGD = 16384,
   RUB = 32768,
   HKD = 65536,
   NOK = 131072,
   PLN = 262144,
   CZK = 524288,
   DKK = 1048576,
   CNH = 2097152,
   OTHER = -1
};
#endif
//+------------------------------------------------------------------+
ENUM_CURRENCY StringToCurrency(string cur)
{
   StringToUpper(cur);
   if(cur=="USD")return USD;
   if(cur=="JPY")return JPY;
   if(cur=="EUR")return EUR;
   if(cur=="GBP")return GBP;
   if(cur=="CAD")return CAD;
   if(cur=="CHF")return CHF;
   if(cur=="AUD")return AUD;
   if(cur=="NZD")return NZD;
   if(cur=="MXN")return MXN;
   if(cur=="SEK")return SEK;
   if(cur=="CNY")return CNY;
   if(cur=="CNH")return CNH;
   if(cur=="ZAR")return ZAR;
   if(cur=="TRY")return TRY;
   if(cur=="SEK")return SEK;
   if(cur=="HUF")return HUF;
   if(cur=="SGD")return SGD;
   if(cur=="RUB")return RUB;
   if(cur=="HKD")return HKD;
   if(cur=="NOK")return NOK;
   if(cur=="PLN")return PLN;
   if(cur=="CZK")return CZK;
   if(cur=="DKK")return DKK;
   return OTHER;
}
//+------------------------------------------------------------------+
ENUM_IMPACT StringToImpact(string imp)
{
   StringToUpper(imp);
   if(imp=="HIGH"    || imp=="IMPACT_HIGH"   )     
      return IMPACT_HIGH;
   if(imp=="MEDIUM"  || imp=="IMPACT_MEDIUM" )   
      return IMPACT_MEDIUM;
   if(imp=="LOW"     || imp=="IMPACT_LOW"    )      
      return IMPACT_LOW;
   if(imp=="HOLIDAY" || imp=="IMPACT_HOLIDAY")  
      return IMPACT_HOLIDAY;
   return IMPACT_NULL;
}
//+------------------------------------------------------------------+