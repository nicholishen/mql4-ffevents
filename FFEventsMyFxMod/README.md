# README #

This is a working demo of the FFEvents library. In order to test this in your MT4 terminal you will need to clone this entire repo into a subfolder in the ../MQL4/Scripts folder and compile with metaeditor.

### What is this repository for? ###

* Demonstration of the new FFEvent lib

### How do I get set up? ###

* Clone repo to a subfolder in the ../MQL4/Scripts folder.
* Compile FFEvents_example script.
* Move lib files to a subfolder in the include folder for further development

### Who do I talk to? ###

* nicholishen@FF || nicholishenFX@reddit

### Filters

*   Keyword - eg. "Unemployment"
*   Keywords - multiple keywords in string array
*   Pair - eg. "USDJPY" = news events for USD and JPY only
*   Currency - use 3 letter abbreviation in all caps as enum. eg. USD
*   Currency flags - Use multiple enums to set bitflags for filter. eg. (USD|JPY|EUR)
*   Impact - use one of the following enums: IMPACT_HIGH, IMPACT_MEDIUM, IMPACT_LOW, IMPACT_HOLIDAY
*   Impact Flags - use a combination of Impact enums to set bitflags. eg. (IMPACT_HIGH|IMPACT_MEDIUM)
*   Number of Minutes leading up to events

### Sorting methods

*   SORT_TIME = Sorts first by future events in ascending order from least time until event, then sorts past events descending by time from event. 
*   SORT_IMPACT = from High - Holiday
*   SORT_CURRENCY = sorts events alphabetically by currency  

### Public Methods of CFFEvents (Events Container class)

*   bool              OnInit();
*   bool              Update();
*   string            Name()               const { return m_name;  }
*   void              Name(const string name)    { m_name = name;  }
*   datetime          LastUpdate()         const;
*   int               Total();
*   void              Clear();
   
*   int               CopyEvents(const CFFEvents &in);
*   int               CopyEvents(const CFFEvents *in);
*   int               operator =(const CFFEvents &in);     
*   int               operator =(const CFFEvents *in);
*   int               AddEvents (const CFFEvents &in);
*   int               AddEvents (const CFFEvents *in);
*   int               operator+=(const CFFEvents &in);
*   int               operator+=(const CFFEvents *in);
   
*   CFFEvents*        Sort(const int mode =0);
*   CFFEvents*        FilterReset();
*   CFFEvents*        FilterByCurrencyFlags(const int           flags);
*   CFFEvents*        FilterByCurrency     (const ENUM_CURRENCY currency);
*   CFFEvents*        FilterByImpactFlags  (const int           flags);
*   CFFEvents*        FilterByImpact       (const ENUM_IMPACT   impact);
*   CFFEvents*        FilterByKeyword      (const string        keyword);
*   CFFEvents*        FilterByKeyword      (const string        &keywords[]);
*   CFFEvents*        FilterByPair         (const string        symbol);
*   CFFEvents*        FilterByMinUntil     (const int           mins);

### Public methods of Events

*   string            Title()        const;
*   string            ToString()     const;
*   String*           ToStringEnhanced();
*   ENUM_CURRENCY     Currency()     const;
*   string            CurrencyStr()  const;
*   ENUM_IMPACT       Impact()       const
*   string            ImpactStr()    const
*   double            Forecast()     const;
*   double            Previous()     const;
*   CFFDatetime       DatetimeStruct()const
*   datetime          Time()         const;   
*   datetime          TimeLocal()    const;
*   int               MinUntil()     const;  
*   string            Unparsed()     const  
*   int               Compare(const CObject *node,const int mode=0)const;