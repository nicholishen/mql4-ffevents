//+------------------------------------------------------------------+
//|                                      FFevents_EA_use_example.mq4 |
//|                                                      nicholishen |
//|                                   www.reddit.com/u/nicholishenFX |
//+------------------------------------------------------------------+
#property copyright "nicholishen"
#property link      "www.reddit.com/u/nicholishenFX"
#property version   "1.00"
#property strict

#include "FFEvents.mqh"
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
CFFEventsMod events;

int OnInit()
{
   if(!events.OnInit())
      return INIT_FAILED;
   events.FilterByImpactFlags(IMPACT_HIGH).Sort(SORT_TIME);
   return INIT_SUCCEEDED;
}
//+------------------------------------------------------------------+

void OnTick()
{
   for(int i=0;i<events.Total();i++)
   {
      int min_until = events[i].MinUntil();
      if( min_until >=0)
      {
         Comment(min_until," minutes until ",events[i].ToString());
         return;
      }   
   }
   Comment("");
}