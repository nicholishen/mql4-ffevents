//+------------------------------------------------------------------+
//|                                                        Event.mqh |
//|                                                      nicholishen |
//|                                   www.reddit.com/u/nicholishenFX |
//+------------------------------------------------------------------+
#property copyright "nicholishen"
#property link      "www.reddit.com/u/nicholishenFX"
#property version   "1.00"
#property strict
#include <Object.mqh>
#include <Tools\DateTime.mqh>
#include "EventParse.mqh"
#include "FFDateTime.mqh"
#include "Common.mqh"
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class Event : public CObject
{
protected:
   string            m_orig;
   string            m_title;
   string            m_currency;
   string            m_impact;
   string            m_forecast;
   string            m_previous;
   CFFDatetime       m_time;
   String           *m_to_string;
public:
                     Event();
                     Event(const string);
                    ~Event();
   bool              Init(const string);
   string            Title()        const { return m_title;          }
   string            ToString()     const;
   String*           ToStringEnhanced();
   ENUM_CURRENCY     Currency()     const { return StringToCurrency(m_currency);}
   string            CurrencyStr()  const { return m_currency;        }
   ENUM_IMPACT       Impact()       const { return StringToImpact(m_impact);}
   string            ImpactStr()    const { return m_impact;         }
   double            Forecast()     const;
   double            Previous()     const;
   CFFDatetime       DatetimeStruct()const{ return m_time;           }
   datetime          Time()         const;   
   datetime          TimeLocal()    const;
   int               MinUntil()     const;  
   string            Unparsed()     const { return m_orig;           } 
   int               Compare(const CObject *node,const int mode=0)const;
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
Event::Event()
{
}
Event::Event(const string event)
{
   Init(event);
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
Event::~Event()
{
   if(CheckPointer(m_to_string)==POINTER_DYNAMIC)
      delete m_to_string;
}
//+------------------------------------------------------------------+

bool Event::Init(const string event)
{
   m_orig      = event;
   EventParse e= event;
   m_title     = e.ParseEvent("<name>");
   m_currency  = e.ParseEvent("<currency>");
   string date = e.ParseEvent("<date>");
   //string time = e.ParseEvent("<time>");
   m_impact    = e.ParseEvent("<impact>");
   if(Impact()!=IMPACT_HIGH)
      return false;
   //m_forecast  = e.ParseEvent("<forecast>");
   m_previous  = e.ParseEvent("<previous>");
   m_time.DateTimeAdj(date);
   return true;
}
//+------------------------------------------------------------------+
datetime Event::Time() const
{ 
   return m_time.DateTime();
}
//+------------------------------------------------------------------+
datetime Event::TimeLocal(void)const
{
   datetime adj = TimeLocal() - TimeCurrent();
   return Time()+adj;
}
//+------------------------------------------------------------------+
int Event::Compare(const CObject *node,const int mode=0)const
{
   Event *other = (Event*)node;
   if(mode==SORT_CURRENCY)
   {
      if(this.Currency()>other.Currency())
         return 1;
      if(this.Currency()<other.Currency())
         return -1;
      return 0;
   }
   if(mode==SORT_IMPACT)
   {
      if(this.Impact() > other.Impact())
         return 1;
      if(this.Impact() < other.Impact())
         return -1;
      return 0;
   }
   // default sort mode sort Ascending on current events, then decending on past events.
   if(this.MinUntil() >= 0 && other.MinUntil() < 0)
      return -1;
   if(this.MinUntil() < 0 && other.MinUntil() <0 )
   {
      if(this.MinUntil() > other.MinUntil())
         return -1;
      if(this.MinUntil() < other.MinUntil())
         return 1;
      return 0;
   }  
   if(this.MinUntil() > other.MinUntil())
      return 1;
   if(this.MinUntil() < other.MinUntil())
      return -1;
   return 0;
}
//+------------------------------------------------------------------+
int Event::MinUntil(void)const
{
   datetime time = m_time.DateTime();
   return int(( time - TimeCurrent()) / 60);
}
//+------------------------------------------------------------------+
double Event::Forecast(void)const
{
   return StringToDouble(m_forecast);
}
//+------------------------------------------------------------------+
double Event::Previous(void)const
{
   return StringToDouble(m_previous);
}
//+------------------------------------------------------------------+
string Event::ToString(void)const
{
   String com,f;
   com ="[{CUR}][{impact}] :: {day-name}, {MM/DD} at {HH:MI} ({status}) :: {title} <<f= {2} : p= {2}>>";
   com.Format(CurrencyStr(),ImpactStr());
   CFFDatetime t = DatetimeStruct();  
   com.Format(t.ShortDayName(),Time(),Time());
   int min = MinUntil();
   com.Format(min>0 ? (f="{} hrs and {} min").Format(min/60,min%60).Str():"Event has passed");
   com.Format(Title(),Forecast(),Previous());
   return com.Str();
}
//+------------------------------------------------------------------+
String* Event::ToStringEnhanced(void)
{
   if(m_to_string == NULL)
      m_to_string = new String;
   m_to_string = ToString();
   return m_to_string;
}
//+------------------------------------------------------------------+