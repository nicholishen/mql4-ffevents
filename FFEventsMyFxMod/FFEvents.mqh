//+------------------------------------------------------------------+
//|                                                     FFEvents.mqh |
//|                                                      nicholishen |
//|                                   www.reddit.com/u/nicholishenFX |
//+------------------------------------------------------------------+
#property copyright "nicholishen"
#property link      "www.reddit.com/u/nicholishenFX"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
#include "Event.mqh"
#include "objvector.mqh"
#include <stdlib.mqh>
//+------------------------------------------------------------------+
//#import "urlmon.dll"
//int URLDownloadToFileW(int pCaller,string szURL,string szFileName,int dwReserved,int Callback);
//#import
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CFFEventsMod : public objvector<Event*>
{
protected:
   bool              m_init;
   string            m_name;
   string            m_xml_file;
   string            m_xml_url;
   string            m_xml_data; 
   datetime          m_last_update;  
   // static members 
   static CFFEventsMod *m_events;
   static int        m_instances;
public:
                     CFFEventsMod();
                     CFFEventsMod(const string name);
                    ~CFFEventsMod();
   bool              OnInit();
   bool              Update();
   string            Name()               const { return m_name;  }
   void              Name(const string name)    { m_name = name;  }
   datetime          LastUpdate()         const;
   int               Total();
   void              Clear();
   
   int               CopyEvents(const CFFEventsMod &in);
   int               CopyEvents(const CFFEventsMod *in);
   int               operator =(const CFFEventsMod &in)  { return CopyEvents(in);   }     
   int               operator =(const CFFEventsMod *in)  { return CopyEvents(in);   }
   int               AddEvents (const CFFEventsMod &in);
   int               AddEvents (const CFFEventsMod *in);
   int               operator+=(const CFFEventsMod &in)  { return AddEvents(in);    }
   int               operator+=(const CFFEventsMod *in)  { return AddEvents(in);    }
   
   CFFEventsMod*        Sort(const int mode =0)    { CArrayObj::Sort(mode); return &this;}
   CFFEventsMod*        FilterReset();
   CFFEventsMod*        FilterByCurrencyFlags(const int           flags);
   CFFEventsMod*        FilterByCurrency     (const ENUM_CURRENCY currency);
   CFFEventsMod*        FilterByImpactFlags  (const int           flags);
   CFFEventsMod*        FilterByImpact       (const ENUM_IMPACT   impact);
   CFFEventsMod*        FilterByKeyword      (const string        keyword);
   CFFEventsMod*        FilterByKeyword      (const string        &keywords[]);
   CFFEventsMod*        FilterByPair         (const string        symbol);
   CFFEventsMod*        FilterByMinUntil     (const int           mins);
protected:
   bool              _XmlDownload();
   bool              _XmlRead();
   void              _ParseXML();
   void              _Init();
   int               _Total()const;
   
};
//+------------------------------------------------------------------+
//--- static members
CFFEventsMod  *CFFEventsMod::m_events     = NULL;
int         CFFEventsMod::m_instances  = 0;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CFFEventsMod::CFFEventsMod():m_init(false)
{
   _Init();
}
//+------------------------------------------------------------------+
CFFEventsMod::CFFEventsMod(string name):m_name(name),m_init(false)
{ 
   _Init();
}
//+------------------------------------------------------------------+
CFFEventsMod::~CFFEventsMod()
{
   if(m_instances <= 2 && CheckPointer(m_events)==POINTER_DYNAMIC && &this != m_events)
   {
      m_events.FreeMode(true);
      delete m_events;
   }
   m_instances--;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CFFEventsMod::_Init()
{
   m_instances++;
   FreeMode(false);
}
//+------------------------------------------------------------------+
bool  CFFEventsMod::OnInit()
{
   m_xml_file  ="calendar_statement.xml";
   m_xml_url   ="http://www.forexfactory.com/ffcal_week_this.xml";
   //if(!TerminalInfoInteger(TERMINAL_DLLS_ALLOWED))
   //{
   //   Alert("CFFEventsMod: Please Allow DLL Imports!");
   //   return false;
   //}
   if(Update())
      return true;
   return false;
}
//+------------------------------------------------------------------+
//| Check for update XML                                             |
//+------------------------------------------------------------------+
bool CFFEventsMod::Update()
{
   //FileDelete(m_xml_file);
   //if(!_XmlDownload())
   //   return false;
   if(!_XmlRead())
      return false;
   PrintFormat("CFFEventsMod: updated successfully! "+string(TimeCurrent()));
   _ParseXML();
   if(m_events==NULL)
      m_events = new CFFEventsMod();
   m_events = this; //operator overload call
   m_init   = true;
   return true;
}
//+------------------------------------------------------------------+      
void CFFEventsMod::_ParseXML(void)
{
   string bTag = "<event>";
   string eTag = "</event>";
   EventParse parse = m_xml_data;
   Clear();
   int i=parse.Find(0,bTag);
   while(parse.Find(i,eTag)>=0 && !IsStopped())
   {  
      Event *event = new Event;
      int end = parse.Find(i,eTag);
      string eString = parse.Substr(i,end);
      if(event.Init(eString))
         this.Add(event);
      else
         delete event;
      i = end + StringLen(eTag);
   }
   int debug_total = this.Total();
   
}
//+-------------------------------------------------------------------------------------------+
//| Download XML file from forexfactory                                                       |    |
//+-------------------------------------------------------------------------------------------+
bool CFFEventsMod::_XmlDownload()
{
   string cookie=NULL,headers; 
   char post[],result[]; 
   int res; 
//--- to enable access to the server, you should add URL "http://www.forexfactory.com" 
//--- in the list of allowed URLs (Main Menu->Tools->Options, tab "Expert Advisors"): 
   string url=m_xml_url; 
//--- Reset the last error code 
   ResetLastError(); 
//--- Loading a html page from Google Finance 
   int timeout=5000; //--- Timeout below 1000 (1 sec.) is not enough for slow Internet connection 
   res=WebRequest("GET",url,cookie,NULL,timeout,post,0,result,headers); 
//--- Checking errors 
   if(res==-1) 
   { 
      Print("Error in WebRequest. Error code  =",GetLastError()); 
      //--- Perhaps the URL is not listed, display a message about the necessity to add the address 
      MessageBox("Add the address 'http://www.forexfactory.com' in the list of allowed URLs on tab 'Tools>Options>Expert Advisors'","Error",MB_ICONINFORMATION); 
      return false;
   } 
   else 
   { 
      //--- Load successfully 
      PrintFormat("The file has been successfully loaded, File size =%d bytes.",ArraySize(result)); 
      //--- Save the data to a file 
      int filehandle=FileOpen(m_xml_file,FILE_WRITE|FILE_BIN); 
      //--- Checking errors 
      if(filehandle!=INVALID_HANDLE) 
      { 
         //--- Save the contents of the result[] array to a file 
         FileWriteArray(filehandle,result,0,ArraySize(result)); 
         //--- Close the file 
         FileClose(filehandle); 
      } 
      else
      {
         Print("Error in FileOpen. Error code=",GetLastError()); 
         return false;
      }
   } 
   return true;
}
//+------------------------------------------------------------------+
//| Read the XML file                                                |
//+------------------------------------------------------------------+
bool CFFEventsMod::_XmlRead()
{
   ResetLastError();
   int FileHandle=FileOpen(m_xml_file,FILE_BIN|FILE_READ);
   if(FileHandle!=INVALID_HANDLE)
   {
      ulong size=FileSize(FileHandle);
      while(!FileIsEnding(FileHandle))
         m_xml_data+=FileReadString(FileHandle,(int)size);
      FileClose(FileHandle);
      return true;
   }    
   PrintFormat("CFFEventsMod: failed to open %s file, Error code = %s",m_xml_file,ErrorDescription(GetLastError()));
   return false;
}
//+------------------------------------------------------------------+
CFFEventsMod* CFFEventsMod::FilterReset()              
{ 
   if(m_events != NULL)
      this = m_events;    //operator overload call   
   return &this;
}
//+------------------------------------------------------------------+
CFFEventsMod* CFFEventsMod::FilterByPair(const string symbol)
{
   int total = Total();
   String s = symbol;
   string bc = s.BaseCurrency();
   string cc = s.CounterCurrency();
   int filter =0; 
   filter  = StringToCurrency(bc)>0 ? filter|StringToCurrency(bc): filter;
   filter  = StringToCurrency(cc)>0 ? filter|StringToCurrency(cc): filter;
   FilterByCurrencyFlags(filter);
   return &this;
}
//+------------------------------------------------------------------+
CFFEventsMod* CFFEventsMod::FilterByImpactFlags(const int flags)
{
   int total = Total();
   CFFEventsMod temp1,temp2;
   int cnt=0;
   for(int c=0;c<4;c++)
   {
      if(bool(flags&BIT(c)))
      {
         temp1 = this;//operator overload call
         temp1.FilterByImpact((ENUM_IMPACT)BIT(c));
         temp2 += temp1;//operator overload call
         cnt++;
      }
   }
   if(cnt==0)
      return &this;
   int debug = temp2._Total();
   CopyEvents(temp2);
   return &this;   
}
//+------------------------------------------------------------------+
CFFEventsMod* CFFEventsMod::FilterByImpact(const ENUM_IMPACT impact)
{
   for(int i=Total()-1;i>=0;i--)
      if(this[i].Impact() != impact)
         Delete(i);
   return &this;   
}
//+------------------------------------------------------------------+
CFFEventsMod* CFFEventsMod::FilterByCurrencyFlags(const int flags)
{  
   int total = Total();
   int cnt=0;
   CFFEventsMod temp1,temp2;
   for(int c=0;c<22;c++)
   {
      if(bool(flags&BIT(c)))
      {
         temp1 = this;//operator overload call
         temp1.FilterByCurrency((ENUM_CURRENCY)BIT(c));
         temp2 += temp1;//operator overload call
         cnt++;
      }
   }
   if(cnt==0)
      return &this;
   this.CopyEvents(temp2);
   return &this;   
}
//+------------------------------------------------------------------+
CFFEventsMod* CFFEventsMod::FilterByCurrency(const ENUM_CURRENCY currency)
{  
   for(int i=Total()-1;i>=0;i--)
      if(this[i].Currency() != currency)
         Delete(i);
   return &this;   
}        
//+------------------------------------------------------------------+  
CFFEventsMod* CFFEventsMod::FilterByKeyword(const string keyword)
{
   for(int i=Total()-1;i>=0;i--)
      if(StringFind(this[i].Title(),keyword)<0)
         Delete(i);
   return &this;
}
//+------------------------------------------------------------------+
CFFEventsMod* CFFEventsMod::FilterByKeyword(const string &keywords[])
{
   int total = Total();
   int cnt=0;
   CFFEventsMod temp1,temp2;
   for(int i=0;i<ArraySize(keywords);i++)
   {
      temp1 = &this;//operator overload call
      temp1.FilterByKeyword(keywords[i]);
      temp2 += temp1;//operator overload call
      cnt++; 
   }
   if(cnt==0)
      return &this;
   this.CopyEvents(temp2);
   return &this;   
}
//+------------------------------------------------------------------+ 
CFFEventsMod* CFFEventsMod::FilterByMinUntil(const int mins)
{
   for(int i=Total()-1;i>=0;i--)
      if(this[i].MinUntil() < 0 || this[i].MinUntil() > mins)
         Delete(i);
   return &this;
}

//+------------------------------------------------------------------+
int CFFEventsMod::CopyEvents(const CFFEventsMod &in)
{
   return CopyEvents(&in);
}
//+------------------------------------------------------------------+
int CFFEventsMod::CopyEvents(const CFFEventsMod *in)
{
   CArrayObj::Clear();
   int c = 0;
   for(int i=0;i<in._Total();i++)
      if(this.Add(in[i]))
         c++;
   m_init = true;
   return c;
}
//+------------------------------------------------------------------+
int CFFEventsMod::AddEvents(const CFFEventsMod &in)
{
   return AddEvents(&in);
}
//+------------------------------------------------------------------+
int CFFEventsMod::AddEvents(const CFFEventsMod *in)
{
   int c=0;
   for(int i=0;i<in._Total();i++)
   {
      bool found = false;
      for(int j=0;j<_Total();j++)
      {
         if(this[j].Title() == in[i].Title() && this[j].Time() == in[i].Time())
         {
            found = true;
            break;
         }
      }
      if(!found)
      {
         if(this.Add(in[i]))
            c++;   
      }
   }
   m_init = true;
   return c;
}
//+------------------------------------------------------------------+
datetime CFFEventsMod::LastUpdate() const
{
   return (datetime)FileGetInteger(m_xml_file,FILE_MODIFY_DATE,false);
}
//+------------------------------------------------------------------+
int CFFEventsMod::Total(void)
{
   int total = CArrayObj::Total();
   if(total == 0 && !m_init)
   {
      total = this = m_events; //operator overload call
      m_init = true;
   }
   return total;
}
//+------------------------------------------------------------------+
int CFFEventsMod::_Total(void)const
{
   return CArrayObj::Total();
}
//+------------------------------------------------------------------+
void CFFEventsMod::Clear()
{
   m_init = false;
   CArrayObj::Clear();
}
//+------------------------------------------------------------------+