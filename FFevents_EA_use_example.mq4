//+------------------------------------------------------------------+
//|                                      FFevents_EA_use_example.mq4 |
//|                                                      nicholishen |
//|                                   www.reddit.com/u/nicholishenFX |
//+------------------------------------------------------------------+
#property copyright "nicholishen"
#property link      "www.reddit.com/u/nicholishenFX"
#property version   "1.00"
#property strict

#include "FFEvents.mqh"
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
{
   CFFEvents events;
   if(!events.OnInit())
      return;
   events.FilterByImpactFlags(IMPACT_HIGH|IMPACT_MEDIUM).Sort(SORT_TIME);
   for(int i=0;i<events.Total();i++)
   {
      if(events[i].MinUntil() <= 30)
      {
         // something here to disable trading
      }   
   }

}
//+------------------------------------------------------------------+
